/**
 * generated by Xtext 2.16.0
 */
package com.project.foo.foo;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Requiered</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.project.foo.foo.Requiered#getRequieredServices <em>Requiered Services</em>}</li>
 * </ul>
 *
 * @see com.project.foo.foo.FooPackage#getRequiered()
 * @model
 * @generated
 */
public interface Requiered extends EObject
{
  /**
   * Returns the value of the '<em><b>Requiered Services</b></em>' containment reference list.
   * The list contents are of type {@link com.project.foo.foo.RequieredService}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Requiered Services</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Requiered Services</em>' containment reference list.
   * @see com.project.foo.foo.FooPackage#getRequiered_RequieredServices()
   * @model containment="true"
   * @generated
   */
  EList<RequieredService> getRequieredServices();

} // Requiered
