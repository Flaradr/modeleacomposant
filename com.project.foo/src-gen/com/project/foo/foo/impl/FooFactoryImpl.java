/**
 * generated by Xtext 2.16.0
 */
package com.project.foo.foo.impl;

import com.project.foo.foo.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FooFactoryImpl extends EFactoryImpl implements FooFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static FooFactory init()
  {
    try
    {
      FooFactory theFooFactory = (FooFactory)EPackage.Registry.INSTANCE.getEFactory(FooPackage.eNS_URI);
      if (theFooFactory != null)
      {
        return theFooFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new FooFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FooFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case FooPackage.DOMAIN_MODEL: return createDomainModel();
      case FooPackage.MODEL: return createModel();
      case FooPackage.IMPORT: return createImport();
      case FooPackage.ASSEMBLY: return createAssembly();
      case FooPackage.COMPONENT_ATTRIBUTE: return createComponentAttribute();
      case FooPackage.BINDING: return createBinding();
      case FooPackage.BINDING_REQUIERED: return createBindingRequiered();
      case FooPackage.BINDING_PROVIDED: return createBindingProvided();
      case FooPackage.COMPONENT: return createComponent();
      case FooPackage.PROVIDED: return createProvided();
      case FooPackage.PROVIDED_SERVICE: return createProvidedService();
      case FooPackage.REQUIERED: return createRequiered();
      case FooPackage.REQUIERED_SERVICE: return createRequieredService();
      case FooPackage.MPROVIDED_SERVICE: return createMProvidedService();
      case FooPackage.PSIGNATURE: return createPSignature();
      case FooPackage.MREQUIERED_SERVICE: return createMRequieredService();
      case FooPackage.RSIGNATURE: return createRSignature();
      case FooPackage.ATTRIBUTE: return createAttribute();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DomainModel createDomainModel()
  {
    DomainModelImpl domainModel = new DomainModelImpl();
    return domainModel;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Model createModel()
  {
    ModelImpl model = new ModelImpl();
    return model;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Import createImport()
  {
    ImportImpl import_ = new ImportImpl();
    return import_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Assembly createAssembly()
  {
    AssemblyImpl assembly = new AssemblyImpl();
    return assembly;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentAttribute createComponentAttribute()
  {
    ComponentAttributeImpl componentAttribute = new ComponentAttributeImpl();
    return componentAttribute;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Binding createBinding()
  {
    BindingImpl binding = new BindingImpl();
    return binding;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BindingRequiered createBindingRequiered()
  {
    BindingRequieredImpl bindingRequiered = new BindingRequieredImpl();
    return bindingRequiered;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BindingProvided createBindingProvided()
  {
    BindingProvidedImpl bindingProvided = new BindingProvidedImpl();
    return bindingProvided;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Component createComponent()
  {
    ComponentImpl component = new ComponentImpl();
    return component;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Provided createProvided()
  {
    ProvidedImpl provided = new ProvidedImpl();
    return provided;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ProvidedService createProvidedService()
  {
    ProvidedServiceImpl providedService = new ProvidedServiceImpl();
    return providedService;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Requiered createRequiered()
  {
    RequieredImpl requiered = new RequieredImpl();
    return requiered;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RequieredService createRequieredService()
  {
    RequieredServiceImpl requieredService = new RequieredServiceImpl();
    return requieredService;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MProvidedService createMProvidedService()
  {
    MProvidedServiceImpl mProvidedService = new MProvidedServiceImpl();
    return mProvidedService;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PSignature createPSignature()
  {
    PSignatureImpl pSignature = new PSignatureImpl();
    return pSignature;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MRequieredService createMRequieredService()
  {
    MRequieredServiceImpl mRequieredService = new MRequieredServiceImpl();
    return mRequieredService;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RSignature createRSignature()
  {
    RSignatureImpl rSignature = new RSignatureImpl();
    return rSignature;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Attribute createAttribute()
  {
    AttributeImpl attribute = new AttributeImpl();
    return attribute;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FooPackage getFooPackage()
  {
    return (FooPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static FooPackage getPackage()
  {
    return FooPackage.eINSTANCE;
  }

} //FooFactoryImpl
