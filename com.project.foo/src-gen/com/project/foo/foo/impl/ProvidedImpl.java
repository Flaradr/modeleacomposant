/**
 * generated by Xtext 2.16.0
 */
package com.project.foo.foo.impl;

import com.project.foo.foo.FooPackage;
import com.project.foo.foo.Provided;
import com.project.foo.foo.ProvidedService;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Provided</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.project.foo.foo.impl.ProvidedImpl#getProvidedServices <em>Provided Services</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProvidedImpl extends MinimalEObjectImpl.Container implements Provided
{
  /**
   * The cached value of the '{@link #getProvidedServices() <em>Provided Services</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getProvidedServices()
   * @generated
   * @ordered
   */
  protected EList<ProvidedService> providedServices;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ProvidedImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return FooPackage.Literals.PROVIDED;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ProvidedService> getProvidedServices()
  {
    if (providedServices == null)
    {
      providedServices = new EObjectContainmentEList<ProvidedService>(ProvidedService.class, this, FooPackage.PROVIDED__PROVIDED_SERVICES);
    }
    return providedServices;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case FooPackage.PROVIDED__PROVIDED_SERVICES:
        return ((InternalEList<?>)getProvidedServices()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case FooPackage.PROVIDED__PROVIDED_SERVICES:
        return getProvidedServices();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case FooPackage.PROVIDED__PROVIDED_SERVICES:
        getProvidedServices().clear();
        getProvidedServices().addAll((Collection<? extends ProvidedService>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case FooPackage.PROVIDED__PROVIDED_SERVICES:
        getProvidedServices().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case FooPackage.PROVIDED__PROVIDED_SERVICES:
        return providedServices != null && !providedServices.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //ProvidedImpl
